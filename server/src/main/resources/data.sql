DELETE FROM book;
ALTER TABLE book AUTO_INCREMENT = 1001;

DELETE FROM category;
ALTER TABLE category AUTO_INCREMENT = 1001;

INSERT INTO `category` (`name`) VALUES ('Horror'),('Romance'),('Mystery'),('Fantasy'), ('Manga'), ('Food');

INSERT INTO `book` (title, author, description, price, rating, is_public, is_featured, category_id)
VALUES
    ('American Psycho', 'Bret Easton Ellis', 'horror', 299, 8, TRUE, FALSE, 1001),
    ('The Amityville Horror', 'Jay Anson', 'horror', 599, 8, FALSE, FALSE, 1001),
    ('The Stand', 'Stephen King', 'horror', 1399, 0, TRUE, FALSE, 1001),
    ('Horror Hotel', 'Fulton & McClaren', 'horror', 499, 0, FALSE, FALSE, 1001),
    ('Frankenstein', 'Mary Shelley', 'horror', 1099, 0, TRUE, FALSE, 1001),
    ('IT', 'Stephen King', 'horror', 1299, 9, FALSE, TRUE, 1001),
    --
    ('The Seven Husbands of Evelyn Hugo', 'Taylor Jenkins Reid', 'romance', 1999, 9, TRUE, TRUE, 1002),
    ('Things We Never Got Over', 'Lucy Score', 'romance', 1899, 0, FALSE, FALSE, 1002),
    ('Every Summer After', 'Carley Fortune', 'romance', 1499, 7, TRUE, FALSE, 1002),
    ('Meant to Be', 'Emily Griffin', 'romance', 1399, 7, FALSE, FALSE, 1002),
    --
    ('The Lost Summers', 'Beatriz Williams', 'mystery', 2499, 7, FALSE, FALSE, 1003),
    ('The Club', 'Ellery Lloyd', 'mystery', 1199, 0, TRUE, FALSE, 1003),
    ('The Perfect Marriage', 'Jevena Rose', 'mystery', 1699, 9, FALSE, FALSE, 1003),
    ('The Da Vinci Code', 'Dan Brown', 'mystery', 199, 8, FALSE, FALSE, 1003),
    --
    ('The Time Traveller''s Wife', 'Audrey Niffenegger', 'fantasy', 1299, 10, TRUE, TRUE, 1004),
    ('The Madness of Crowds', 'Louise Penny', 'fantasy', 2199, 0, TRUE, TRUE, 1004),
    ('The Hobbit', 'J.R.R Tolkien', 'fantasy', 1699, 0, TRUE, FALSE, 1004),
    ('Dune', 'Frank Herbert', 'fantasy', 599, 7, FALSE, FALSE, 1004),
    --
    ('Berserk', 'Kentaro Miura', 'manga', 799, 0, TRUE, FALSE, 1005),
    ('Jujutsu Kaisen', 'Gege Akutami', 'manga', 499, 6, FALSE, FALSE, 1005),
    ('Demon Slayer', 'Kimetsu No Yaiba', 'manga', 599, 0, TRUE, FALSE, 1005),
    ('Attack on Titan', 'Hajime Isayama', 'manga', 1099, 9, FALSE, FALSE, 1005),
    --
    ('Complete Mediterranean Cookbook', 'America''s Test Kitchen', 'cuisine', 3499, 7, FALSE, FALSE, 1006),
    ('Project Smoke', 'Steven Raichlen', 'cuisine', 2099, 0, TRUE, FALSE, 1006),
    ('Crying in H Mart: A Memoir', 'Michelle Zauner', 'cuisine', 1399, 0, FALSE, FALSE, 1006),
    ('The Elements of Pizza', 'Ken Forkish', 'cuisine', 2899, 9, FALSE, FALSE, 1006);

