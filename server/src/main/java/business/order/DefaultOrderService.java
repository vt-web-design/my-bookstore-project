package business.order;

import api.ApiException;
import business.BookstoreDbException;
import business.JdbcUtils;
import business.book.Book;
import business.book.BookDao;
import business.cart.ShoppingCart;
import business.cart.ShoppingCartItem;
import business.customer.Customer;
import business.customer.CustomerDao;
import business.customer.CustomerForm;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DefaultOrderService implements OrderService {

	private BookDao bookDao;
	private OrderDao orderDao;
	private CustomerDao customerDao;
	private LineItemDao lineItemDao;

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	public void setOrderDao(OrderDao orderDao) { this.orderDao = orderDao; }
	public void setCustomerDao(CustomerDao customerDao) { this.customerDao = customerDao; }
	public void setLineItemDao(LineItemDao lineItemDao) { this.lineItemDao = lineItemDao; }

	@Override
	public OrderDetails getOrderDetails(long orderId) {
		Order order = orderDao.findByOrderId(orderId);
		Customer customer = customerDao.findByCustomerId(order.getCustomerId());
		List<LineItem> lineItems = lineItemDao.findByOrderId(orderId);
		List<Book> books = lineItems
				.stream()
				.map(lineItem -> bookDao.findByBookId(lineItem.getBookId()))
				.collect(Collectors.toList());
		return new OrderDetails(order, customer, lineItems, books);
	}

	@Override
    public long placeOrder(CustomerForm customerForm, ShoppingCart cart) {

		validateCustomer(customerForm);
		validateCart(cart);

		// NOTE: MORE CODE PROVIDED NEXT PROJECT
		try (Connection connection = JdbcUtils.getConnection()) {
			Date date = getDate(
					customerForm.getCcExpiryMonth(),
					customerForm.getCcExpiryYear());
			return performPlaceOrderTransaction(
					customerForm.getName(),
					customerForm.getAddress(),
					customerForm.getPhone(),
					customerForm.getEmail(),
					customerForm.getCcNumber(),
					date, cart, connection);
		} catch (SQLException e) {
			throw new BookstoreDbException("Error during close connection for customer order", e);
		}
	}

	private long performPlaceOrderTransaction(
			String name, String address, String phone,
			String email, String ccNumber, Date date,
			ShoppingCart cart, Connection connection) {
		try {
			connection.setAutoCommit(false);
			long customerId = customerDao.create(
					connection, name, address, phone, email,
					ccNumber, date);
			long customerOrderId = orderDao.create(
					connection,
					cart.getComputedSubtotal() + cart.getSurcharge(),
					generateConfirmationNumber(), customerId);
			for (ShoppingCartItem item : cart.getItems()) {
				lineItemDao.create(connection, customerOrderId,
						item.getBookId(), item.getQuantity());
			}
			connection.commit();
			return customerOrderId;
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				throw new BookstoreDbException("Failed to roll back transaction", e1);
			}
			return 0;
		}
	}

	private int generateConfirmationNumber() {
		Random random = new Random();
		return random.nextInt(999999999);
	}

	private Date getDate(String monthString, String yearString) {
		Calendar cal = Calendar.getInstance();
		int day = cal.getActualMaximum(Calendar.DATE);
		String date = monthString + "/" + day + "/" + yearString;
		return new Date(date);
	}

	private void validateCustomer(CustomerForm customerForm) {

		// Name
    	String name = customerForm.getName();
		nameValidations(name);

		// Address
		String address = customerForm.getAddress();
		addressValidations(address);

		// Phone
		String phone = customerForm.getPhone();
		if(phone != null) {
			phone = phone.replace("-", "");
			phone = phone.replace(" ", "");
			phone = phone.replace("(", "");
			phone = phone.replace(")", "");
		}
		phoneValidations(phone);

		// Email
		String email = customerForm.getEmail();
		emailValidations(email);

		// C.C
		String ccNumber = customerForm.getCcNumber();
		if(ccNumber != null) {
			ccNumber = ccNumber.replace(" ", "");
			ccNumber = ccNumber.replace("-", "");
		}
		ccNumberValidations(ccNumber);

		if (expiryDateIsInvalid(customerForm.getCcExpiryMonth(), customerForm.getCcExpiryYear())) {
			throw new ApiException.InvalidParameter("Invalid CC expiry date");

		}
	}

	private void nameValidations(String name){

		if (name == null || name.equals("") || !(name.length() >= 4 && name.length() <= 45)) {
			if(name == null) {
				throw new ApiException.InvalidParameter("Invalid field: NAME IS NULL");
			}
			else if(name.equals("")) {
				throw new ApiException.InvalidParameter("Invalid field: NAME IS EMPTY");
			}
			else if(!(name.length() >= 4 && name.length() <= 45))
			{
				throw new ApiException.InvalidParameter("Invalid field: NAME LENGTH");
			}
			else {
				throw new ApiException.InvalidParameter("Invalid NAME field!");
			}
		}
	}

	private void addressValidations(String address) {

		if(address == null || address.equals("") || !(address.length() >= 4 && address.length() <= 45)) {
			if(address == null) {
				throw new ApiException.InvalidParameter("Invalid field: ADDRESS IS NULL");
			}
			else if(address.equals("")){
				throw new ApiException.InvalidParameter("Invalid field: ADDRESS IS EMPTY");
			}
			else if(!(address.length() >= 4 && address.length() <= 45)){
				throw new ApiException.InvalidParameter("Invalid field: ADDRESS LENGTH");
			}
			else {
				throw new ApiException.InvalidParameter("Invalid ADDRESS field!");
			}
		}
	}

	private void phoneValidations(String phone) {

		if(phone == null || phone.equals("") || phone.length() != 10) {
			if(phone == null) {
				throw new ApiException.InvalidParameter("Invalid field: PHONE IS NULL");
			}
			else if(phone.equals("")){
				throw new ApiException.InvalidParameter("Invalid field: PHONE IS EMPTY");
			}
			else if(phone.length() != 10){
				throw new ApiException.InvalidParameter("Invalid field: PHONE LENGTH - " + phone);
			}
			else {
				throw new ApiException.InvalidParameter("Invalid PHONE field");
			}
		}
	}

	private void emailValidations(String email) {

		if(email == null || email.equals("") || email.contains(" ")
				|| !email.contains("@") || email.charAt(email.length()-1) == '.') {
			if(email == null) {
				throw new ApiException.InvalidParameter("Invalid field: EMAIL IS NULL");
			}
			else if(email.equals("")) {
				throw new ApiException.InvalidParameter("Invalid field: EMAIL IS EMPTY");
			}
			else if(email.contains(" ")) {
				throw new ApiException.InvalidParameter("Invalid field: EMAIL CONTAINS A SPACE");
			}
			else if(!email.contains("@")) {
				throw new ApiException.InvalidParameter("Invalid field: EMAIL DOES NOT CONTAIN '@'");
			}
			else if(email.charAt(email.length()-1) == '.') {
				throw new ApiException.InvalidParameter("Invalid field: EMAIL LAST CHARACTER IS '.'");
			}
			else {
				throw new ApiException.InvalidParameter("Invalid EMAIL field");
			}
		}
	}

	private void ccNumberValidations(String ccNumber) {

		if(ccNumber == null || ccNumber.equals("") || !(ccNumber.length() >= 14 && ccNumber.length() <= 16)) {
			if(ccNumber == null) {
				throw new ApiException.InvalidParameter("Invalid field: CC NUMBER IS NULL");
			}
			else if(ccNumber.equals("")){
				throw new ApiException.InvalidParameter("Invalid field: CC NUMBER IS EMPTY");
			}
			else if(!(ccNumber.length() >= 14 && ccNumber.length() <= 16)){
				throw new ApiException.InvalidParameter("Invalid field: CC NUMBER LENGTH");
			}
			else {
				throw new ApiException.InvalidParameter("Invalid CC NUMBER field");
			}
		}
	}

	private boolean expiryDateIsInvalid(String ccExpiryMonth, String ccExpiryYear) {

		int month = 0;
		int year = 0;
		if(ccExpiryMonth != null && !ccExpiryMonth.equals("")) {
			month = Integer.parseInt(ccExpiryMonth);
		}
		if(ccExpiryYear != null && !ccExpiryYear.equals("")) {
			year = Integer.parseInt(ccExpiryYear);
		}

		if(ccExpiryMonth == null || ccExpiryMonth.equals("") || month < LocalDate.now().getMonthValue()){
			return true;
		}
		if(ccExpiryYear == null || ccExpiryYear.equals("") || year < LocalDate.now().getYear()){
			return true;
		}

		return false;
	}

	private void validateCart(ShoppingCart cart) {

		if(cart == null) {
			throw new ApiException.InvalidParameter("Invalid value: Cart is null");
		}

		if (cart.getItems().size() <= 0) {
			throw new ApiException.InvalidParameter("Invalid value: Cart is empty");
		}

		cart.getItems().forEach(item-> {
			if (item.getQuantity() < 1 || item.getQuantity() > 99) {
				throw new ApiException.InvalidParameter("Invalid value: Cart item quantity");
			}
			Book databaseBook = bookDao.findByBookId(item.getBookId());

			if(databaseBook.getPrice() != item.getBookForm().getPrice()){
				throw new ApiException.InvalidParameter("Invalid value: Item price");
			}

			if(databaseBook.getCategoryId() != item.getBookForm().getCategoryId()){
				throw new ApiException.InvalidParameter("Invalid value: Item category");
			}
		});
	}
}
